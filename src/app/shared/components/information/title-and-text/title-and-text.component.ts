import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-title-and-text',
  templateUrl: './title-and-text.component.html',
  styleUrls: ['./title-and-text.component.scss']
})
export class TitleAndTextComponent implements OnInit {
  
  @Input()
  title!: string;
  
  @Input()
  text!: string;

  @Input()
  icon!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
