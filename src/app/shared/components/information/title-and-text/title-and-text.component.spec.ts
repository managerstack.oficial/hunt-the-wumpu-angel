import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TitleAndTextComponent } from './title-and-text.component';

describe('TitleAndTextComponent', () => {
  let component: TitleAndTextComponent;
  let fixture: ComponentFixture<TitleAndTextComponent>;
  let title = 'My title'; 
  let text = 'My text'
  let icon = 'assets/images/monster.png'

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TitleAndTextComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TitleAndTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correctly render the passed @Input', () => {
    component.title = title;
    component.text = text;
    component.icon = icon;
    fixture.detectChanges(); 
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h3').textContent).toBe(title);
    expect(compiled.querySelector('div.text').textContent).toBe(text);
    expect(compiled.querySelector('img.icon').getAttribute('src')).toBe(icon);
  });

  it('should correctly render only title @Input', () => {
    component.title = title;
    fixture.detectChanges(); 
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h3').textContent).toBe(title);
    expect(compiled.querySelector('div.text')).toBe(null);
    expect(compiled.querySelector('img.icon')).toBe(null);
  });

  it('should correctly render only text @Input', () => {
    component.text = text;
    fixture.detectChanges(); 
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h3')).toBe(null);
    expect(compiled.querySelector('div.text').textContent).toBe(text);
    expect(compiled.querySelector('img.icon')).toBe(null);
  });

  it('should correctly render only icon @Input', () => {
    component.icon = icon;
    fixture.detectChanges(); 
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h3')).toBe(null);
    expect(compiled.querySelector('div.text')).toBe(null);
    expect(compiled.querySelector('img.icon').getAttribute('src')).toBe(icon);
  });


});
