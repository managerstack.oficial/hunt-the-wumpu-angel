import { Component, OnInit, Output, EventEmitter,Input } from '@angular/core';
@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input()
  title!: string;

  @Input()
  icon!: string;

  @Output() clickButton:EventEmitter<any> = new EventEmitter<any>()
  
  constructor() { }

  ngOnInit(): void {}

}
