import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ButtonComponent } from './button.component';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let title = 'My title'; 
  let icon = 'assets/images/monster.png'

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should correctly render only title @Input', () => {
    component.title = title;
    fixture.detectChanges(); 
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('span').textContent).toBe(title);
  });

  it('should correctly @Output a event', () => {

    component.title = title;
    component.icon = icon;
    spyOn(component.clickButton, 'emit');

    const button = fixture.nativeElement.querySelector('button');
    button.click();
    fixture.detectChanges();

    expect(component.clickButton.emit).toHaveBeenCalledWith();

  });

});
