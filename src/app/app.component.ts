import { Component, OnInit } from '@angular/core';
import { GameService } from './game/services/game.service';
import { Subscription } from 'rxjs';
import { GameData } from './game/services/constants'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = '';
  status = '';
  gameData!: GameData;
  private serviceSubscription: Subscription | undefined;
  

  constructor(public enviroment: GameService) { }

  ngOnInit(): void {
    this.statusConfigurations();
    this.serviceSubscription = this.enviroment.dataBoard.subscribe(
      (data) => { 
        this.gameData = data  as GameData;
      }
    );
  }

  ngOnDestroy(): void {
    this.serviceSubscription?.unsubscribe();
  }

  statusConfigurations(){
    this.status = "configuration";
  }

  statusGamePlay(){
    this.status = "game";
  }

}
