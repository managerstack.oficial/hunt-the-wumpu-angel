import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';

import { 
  HomeComponent, 
  GameBoardComponent,
  ControlPanelComponent,
  ControlPanelLegendComponent,
  ControlPanelStatiticsComponent,
  ControlPanelControlsComponent,
  ControlPanelEndComponent
 } from './game/components';

import { TitleAndTextComponent } from './shared/components/information/title-and-text/title-and-text.component';
import { ButtonComponent } from './shared/components/input/button/button.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GameBoardComponent,
    ControlPanelComponent,
    ControlPanelLegendComponent,
    ControlPanelControlsComponent,
    ControlPanelStatiticsComponent,
    TitleAndTextComponent,
    ButtonComponent,
    ControlPanelEndComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
