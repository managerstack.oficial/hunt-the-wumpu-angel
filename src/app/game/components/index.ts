export * from './game-board/game-board.component';
export * from './home/home.component';
export * from './control-panel/control-panel.component'
export * from './control-panel/control-panel-legend/control-panel-legend.component'
export * from './control-panel/control-panel-controls/control-panel-controls.component'
export * from './control-panel/control-panel-statitics/control-panel-statitics.component'
export * from './control-panel/control-panel-end/control-panel-end.component'