import { Component, OnInit, Input } from '@angular/core';
import { GameData } from '../../../services/constants'

@Component({
  selector: 'app-control-panel-statitics',
  templateUrl: './control-panel-statitics.component.html',
  styleUrls: ['./control-panel-statitics.component.scss']
})
export class ControlPanelStatiticsComponent implements OnInit {

  @Input()
  gameData!: GameData;
  
  status = ""

  descriptions = {
    ["top"]: "Arriba",
    ["left"]: "Izquierdo",
    ["right"]: "Derecho",
    ["down"]: "Abajo"
  };

  constructor() { }

  ngOnInit(): void {
    
  }

  getGold(): string{
    return (this.gameData.goldTaken || '0') +" / "+ (this.gameData.goldNumber || '0');
  }

  getDirection(): string{
    let d = this.gameData.direction;
    const map: {[key:string]: string} = this.descriptions;
    return map[d] || "";
  }

}
