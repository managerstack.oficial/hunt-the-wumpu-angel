import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ControlPanelStatiticsComponent } from './control-panel-statitics.component';
import {boardModelsDefault} from '../../../services/constants'
import { TitleAndTextComponent } from '../../../../shared/components/information/title-and-text/title-and-text.component';

describe('ControlPanelStatiticsComponent', () => {

  let component: ControlPanelStatiticsComponent;
  let fixture: ComponentFixture<ControlPanelStatiticsComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlPanelStatiticsComponent, TitleAndTextComponent ],

    })
    .compileComponents();


    fixture = TestBed.createComponent(ControlPanelStatiticsComponent);
    component = fixture.componentInstance;
    component.gameData = boardModelsDefault;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render lives, and gold ', () => {
    
    let counter = 0;
    let live = 1;
    let arrow = 3;

    component.gameData.arrows = arrow;
    component.gameData.lives = live;
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    const texts = compiled.querySelectorAll('.information-title-and-text .text')

    for (let i = 0; i < texts.length; ++i) {

      if (live == texts[i].textContent || arrow == texts[i].textContent)
        counter++;

      
    }
    expect(2).toBe(counter);
  });
});
