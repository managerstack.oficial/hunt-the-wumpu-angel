import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GameService } from '../../services/game.service';
import { GameData, NOTIFICATIONS } from '../../services/constants'

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss']
})
export class ControlPanelComponent implements OnInit {

  @Input()
  enviroment!: GameService;

  @Input()
  gameData!: GameData;

  @Output() clickButtonRestar:EventEmitter<any> = new EventEmitter<any>()
  
  status = ""
  win = NOTIFICATIONS.youWin;
  dead = NOTIFICATIONS.youDead;

  ngOnInit(): void {
    this.statusControlAndStatistics();
  }

  statusLegend(){
    this.status = "legend"
  }

  statusControlAndStatistics(){
    this.status = "controlandstatistics"
  }


}
