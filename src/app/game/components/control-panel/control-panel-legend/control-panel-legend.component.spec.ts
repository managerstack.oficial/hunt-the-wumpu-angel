import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlPanelLegendComponent } from './control-panel-legend.component';

describe('ControlPanelLegendComponent', () => {
  let component: ControlPanelLegendComponent;
  let fixture: ComponentFixture<ControlPanelLegendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlPanelLegendComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ControlPanelLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render all the legends', () => {
    
    const compiled = fixture.debugElement.nativeElement;
    const l1 = compiled.querySelectorAll('.control-desing').length;
    const l2 = compiled.querySelectorAll('.superhero-desing').length;
    const l3 = compiled.querySelectorAll('.enviroment-desing').length;


    expect(0).toBeLessThan(l1);
    expect(0).toBeLessThan(l2);
    expect(0).toBeLessThan(l3);

  });


});
