import { Component, OnInit, Input } from '@angular/core';
import { GameService } from '../../../services/game.service';
import { 
  CONSTROLSDESING,
  SUPERHERODESINGS,
  ENVIROMENTDESINGS, } from '../../../services/constants'

@Component({
  selector: 'app-control-panel-legend',
  templateUrl: './control-panel-legend.component.html',
  styleUrls: ['./control-panel-legend.component.scss']
})
export class ControlPanelLegendComponent implements OnInit {

  @Input()
  enviroment!: GameService;
  CONTROLSDESING = CONSTROLSDESING.Legend;
  SUPERHERODESINGS = SUPERHERODESINGS.Legend;
  ENVIROMENTDESINGS = ENVIROMENTDESINGS.Legend

  constructor() { }

  ngOnInit(): void {
    
  }

}
