import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-control-panel-end',
  templateUrl: './control-panel-end.component.html',
  styleUrls: ['./control-panel-end.component.scss']
})
export class ControlPanelEndComponent implements OnInit {

  @Input()
  title!: string;

  @Input()
  text!: string;

  @Input()
  url!: string;

  @Output() clickButton:EventEmitter<any> = new EventEmitter<any>()

  constructor() { }

  ngOnInit(): void {
  }

  goToHome(){
    this.clickButton.emit();
  }
}
