import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlPanelEndComponent } from './control-panel-end.component';
import { TitleAndTextComponent } from '../../../../shared/components/information/title-and-text/title-and-text.component';
import { ButtonComponent } from '../../../../shared/components/input/button/button.component';


describe('ControlPanelEndComponent', () => {
  let component: ControlPanelEndComponent;
  let fixture: ComponentFixture<ControlPanelEndComponent>;
  let title = 'My title'; 
  let text = 'My text'
  let icon = 'assets/images/monster.png'

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlPanelEndComponent, TitleAndTextComponent, ButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ControlPanelEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title, text and img', () => {
    expect(component).toBeTruthy();
  });


  it('should correctly render only title, text and img @Input', () => {

    component.title = title;
    component.text = text;
    component.url = icon
    fixture.detectChanges(); 

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.title').textContent).toBe(title);
    expect(compiled.querySelector('.text').textContent).toBe(text);
    expect(compiled.querySelector('.img').getAttribute('src')).toBe(icon);
  });

  it('should correctly @Output a event', () => {

    component.title = title;
    component.text = text;
    component.url = icon

    spyOn(component.clickButton, 'emit');
    const button = fixture.nativeElement.querySelector('button');
    button.click();

    fixture.detectChanges();
    expect(component.clickButton.emit).toHaveBeenCalledWith();

  });

});
