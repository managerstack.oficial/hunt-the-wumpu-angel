import { Component, OnInit, Input } from '@angular/core';
import { GameService } from '../../../services/game.service';
import { CONSTROLSDESING } from '../../../services/constants'


@Component({
  selector: 'app-control-panel-controls',
  templateUrl: './control-panel-controls.component.html',
  styleUrls: ['./control-panel-controls.component.scss']
})
export class ControlPanelControlsComponent implements OnInit {

  @Input()
  enviroment!: GameService;
  CONTROLSDESING = CONSTROLSDESING;

  constructor() { }

  ngOnInit(): void {}  

  goTurnBackLeft(){ this.enviroment.gameTurnBack(CONSTROLSDESING.pressLeft);}

  goRunTop(){ this.enviroment.gameKeyboardActions(38);}

  goTurnBackRight(){ this.enviroment.gameTurnBack(CONSTROLSDESING.pressRight);}

  goRunLeft(){ this.enviroment.gameKeyboardActions(37);}

  goTakeSomenthing(){ this.enviroment.gameGetGold();}

  goRunRight(){ this.enviroment.gameKeyboardActions(39);}

  goShoot(){ this.enviroment.gameLaunchArrow();}

  goRunDown(){ this.enviroment.gameKeyboardActions(40);}

}
