import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlPanelControlsComponent } from './control-panel-controls.component';
import { TitleAndTextComponent } from '../../../../shared/components/information/title-and-text/title-and-text.component';
import { ButtonComponent } from '../../../../shared/components/input/button/button.component';


describe('ControlPanelControlsComponent', () => {
  let component: ControlPanelControlsComponent;
  let fixture: ComponentFixture<ControlPanelControlsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlPanelControlsComponent, TitleAndTextComponent, ButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ControlPanelControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
