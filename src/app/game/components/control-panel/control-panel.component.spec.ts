import { ComponentFixture, TestBed } from '@angular/core/testing';
import {boardModelsDefault} from '../../services/constants'
import { ControlPanelComponent } from './control-panel.component';
import { TitleAndTextComponent } from '../../../shared/components/information/title-and-text/title-and-text.component';
import { ButtonComponent } from '../../../shared/components/input/button/button.component';
import { GameService } from '../../services/game.service';


import { 
  ControlPanelLegendComponent,
  ControlPanelStatiticsComponent,
  ControlPanelControlsComponent,
  ControlPanelEndComponent
 } from '../../components';


describe('ControlPanelComponent', () => {
  let component: ControlPanelComponent;
  let fixture: ComponentFixture<ControlPanelComponent>;
  let gameService: GameService;
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        ControlPanelComponent, 
        TitleAndTextComponent, 
        ButtonComponent,
        ControlPanelLegendComponent,
        ControlPanelStatiticsComponent,
        ControlPanelControlsComponent,
        ControlPanelEndComponent ]
    })
    .compileComponents();

    gameService = TestBed.inject(GameService);
    fixture = TestBed.createComponent(ControlPanelComponent);
    component = fixture.componentInstance;
    component.gameData = boardModelsDefault;
    component.enviroment = gameService;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render statistic screen', () => {
    component.gameData.lives = 1;
    component.gameData.win = false;
    component.status = "controlandstatistics";
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.control-panel-statitics')).not.toBe(null);
  });

  it('should render controls screen', () => {

    component.gameData.lives = 1;
    component.gameData.win = false;
    component.status = "controlandstatistics";
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.control-panel-controls')).not.toBe(null);
  });

  it('should render legend screen', () => {

    component.gameData.lives = 1;
    component.gameData.win = false;
    component.status = "legend";
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.control-panel-controls-container')).not.toBe(null);
  });

  it('should render win screen', () => {
    component.gameData.lives = 1;
    component.gameData.win = true;
    component.status = "legend";
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.end-game')).not.toBe(null);
  });

  it('should render lost screen', () => {

    component.gameData.lives = 0;
    component.gameData.win = false;
    component.status = "legend";
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.end-game')).not.toBe(null);
  });

});
