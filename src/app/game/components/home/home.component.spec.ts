import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule }   from '@angular/forms';
import { HomeComponent } from './home.component';
import { GameService } from '../../services/game.service';
import { CONSTROLSDESING } from '../../services/constants'

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let gameService: GameService;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ HomeComponent ],
      providers: [GameService]
    })
    .compileComponents();


    gameService = TestBed.inject(GameService);
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should check @Input gameService is working', () => {
    
    component.enviroment = gameService;
    fixture.detectChanges(); 
    
    expect(component.enviroment.arrows).toBe(0);
    expect(component.enviroment.direction).toBe(CONSTROLSDESING.top);
    
  });

  it('should check is gameService change from component', () => {
    
    let lives = 1;
    let monster = 5;
    let holes = 10;
    let golds = 2;
    let arrow = 4;
    let x = 15;
    let y = 15;

    component.enviroment = gameService;
    component.enviroment.gameRestart();
    component.enviroment.setLives(lives);
    component.enviroment.setMonsters(monster);
    component.enviroment.setHoles(holes);
    component.enviroment.setGolds(golds);
    component.enviroment.setArrows(arrow);
    component.enviroment.setEnvironmentX(x);
    component.enviroment.setEnvironmentY(x);
    fixture.detectChanges(); 
    
    expect(gameService.lives).toBe(lives);
    expect(gameService.boardModels.monstersNumbers).toBe(monster);
    expect(gameService.boardModels.holesPercents).toBe(holes);
    expect(gameService.boardModels.goldNumber).toBe(golds);
    expect(gameService.arrows).toBe(arrow);
    expect(gameService.boardModels.rows).toBe(x);
    expect(gameService.boardModels.cols).toBe(y);
    
  });

  it('should correctly set standar game and @Output game play', () => {

    component.enviroment = gameService;
    component.gamePlayMedium();
    spyOn(component.clickPlayGame, 'emit');

    const button = fixture.nativeElement.querySelector('button.fast');
    button.click();
    fixture.detectChanges();

    expect(gameService.lives).toBe(component.gameFast.lives);
    expect(component.clickPlayGame.emit).toHaveBeenCalledWith();

  });

});
