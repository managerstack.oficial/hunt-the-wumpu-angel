import { Component, OnInit, Output, EventEmitter,Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GameService } from '../../services/game.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @Input()
  enviroment!: GameService;

  @Output() clickPlayGame:EventEmitter<any> = new EventEmitter<any>()
  
  gameFast = {
    lives: 2,
    monsters: 20,
    holes: 10,
    golds: 2,
    arrows: 40,
    x: 15,
    y: 15
  }

  ngOnInit(): void {}

  gamePlayCustom(f:NgForm){

    this.enviroment.gameRestart();
    this.enviroment.setLives(f.value.lives);
    this.enviroment.setMonsters(f.value.monsters);
    this.enviroment.setHoles(f.value.holes);
    this.enviroment.setGolds(f.value.golds);
    this.enviroment.setArrows(f.value.arrows);
    this.enviroment.setEnvironmentX(f.value.xPosition);
    this.enviroment.setEnvironmentY(f.value.yPosition);
    this.clickPlayGame.emit();
  }

  gamePlayMedium(){
    this.enviroment.gameRestart();
    this.enviroment.setLives(this.gameFast.lives);
    this.enviroment.setMonsters(this.gameFast.monsters);
    this.enviroment.setHoles(this.gameFast.holes);
    this.enviroment.setGolds(this.gameFast.golds);
    this.enviroment.setArrows(this.gameFast.arrows);
    this.enviroment.setEnvironmentX(this.gameFast.x);
    this.enviroment.setEnvironmentY(this.gameFast.y);
    this.clickPlayGame.emit();
  }

}
