import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBoardComponent } from './game-board.component';
import { GameService } from '../../services/game.service';

describe('GameBoardComponent', () => {
  let component: GameBoardComponent;
  let fixture: ComponentFixture<GameBoardComponent>;
  let gameService: GameService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameBoardComponent ],
      providers: [GameService]
    })
    .compileComponents();

    gameService = TestBed.inject(GameService);
    fixture = TestBed.createComponent(GameBoardComponent);
    component = fixture.componentInstance;
    component.enviroment = gameService;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
