import {
  ElementRef,
  OnInit,
  Component,
  HostListener, 
  ViewChild, 
  Input } from '@angular/core';

import { Subscription } from 'rxjs';
import { GameService } from '../../services/game.service';
import { GameData, RenderPosition } from '../../services/constants'

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.scss']
})
export class GameBoardComponent implements OnInit {

  @Input()
  enviroment!: GameService;
  
  @ViewChild('spaceBoard', { static: true }) 
  spaceBoard!: ElementRef<HTMLCanvasElement>;
  private ctx!: CanvasRenderingContext2D | null;
  private serviceSubscription: Subscription | undefined;

  /**
   * 
   * @param service game services all operations
   */
  constructor() {}

  ngOnInit(): void {
    this.gamePlay();
    this.serviceSubscription = this.enviroment.dataBoard.subscribe(
      (data) => { 
        this.gameUpdate(data  as GameData);
      }
    );
  }

  @HostListener('window:keydown', ['$event'])
  keyEvent(event: KeyboardEvent){
    this.enviroment.gameKeyboardActions(event.keyCode);
  }

  gameUpdate(response: GameData){

    if (response.lastLaunch.url != ''){

      this.setGameBoardModelsDraw(
          response.lastLaunch.x, 
          response.lastLaunch.y, 
          response.lastLaunch.url);

      this.enviroment.gameLaunchClear();
        
    }else{
      
      this.setGameBoardModelsDraw(
        response.lastVisited.x,
        response.lastVisited.y, 
        response.lastVisited.url);
  
      this.setGameBoardModelsDraw(
        response.newVisited.x, 
        response.newVisited.y, 
        response.newVisited.url);
  
    }

  }


  /** GAMEPLAY ACTIONS */

  gamePlay(): void {
    let response = this.enviroment.gamePlay();
    this.setGameRenderBoard();
    this.setGameBoardModelsDraw(
      response.x, 
      response.y, 
      response.url);
  }

  gameReset(): void{}

  /************************** */
  /** MANAGER DRAW ENVIROMENT */


  /**
   * Use to make the initial render in our board
   */
  private setGameRenderBoard(): Boolean{
    let element = (this.spaceBoard.nativeElement as HTMLCanvasElement);
    this.ctx = element.getContext('2d');

    if (this.ctx == null)
      return false

    // Calculate size of canvas from constants.
    this.ctx.canvas.width = this.enviroment.getEnvironmentX() * this.enviroment.getBlockSize();
    this.ctx.canvas.height = this.enviroment.getEnvironmentY() * this.enviroment.getBlockSize();
    
    // Scale so we don't need to give size on every draw.
    this.ctx.scale(this.enviroment.getBlockSize(), this.enviroment.getBlockSize());
    return true
  }


  private setGameBoardModelsDraw(x: number, y: number, url: string){

    if (this.ctx != null)
      this.ctx.clearRect(x, y, 1, 1);

    var img = new Image();
    img.onload = () => {
      
      if (this.ctx != null){
        this.ctx.clearRect(x, y, 1, 1);
        this.ctx.drawImage(img, x, y, 1, 1);
      }
    }
    img.src = url;
  }


}
