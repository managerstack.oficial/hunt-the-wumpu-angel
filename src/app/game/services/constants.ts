
export interface RenderPosition {
  x: number;
  y: number;
  url: string;
}

export interface Character {
  x: number;
  y: number;
  xLast: number;
  yLast: number;
  environment: Array<number>;
  arrows: number,
  lives: number;
  direction: string;
}

export var boardModelsDefault = {
  gold: 0,
  goldNumber: 0,
  goldTaken: 0,
  monsters:0,
  monstersNumbers: 0,
  holesPercents: 0,
  cols: 0,
  rows: 0,
  lives: 0,
  arrows: 0,
  win:false,
  direction: 'top',
  lastLaunch: { x: 0, y: 0, url:''},
  lastVisited: { x: 0, y: 0, url:''},
  newVisited: { x: 0, y: 0, url:''}
}

export interface GameData {
  gold: number;
  goldNumber: number;
  goldTaken: number;
  monsters:number;
  monstersNumbers: number;
  holesPercents: number;
  cols: number;
  rows: number;
  lives: number;
  arrows: number;
  win: boolean;
  direction: string;
  lastLaunch:{ 
    x: number;
    y: number;
    url: string;
  },
  lastVisited: {
    x: number;
    y: number;
    url: string;
  }
  newVisited: {
    x: number;
    y: number;
    url: string;
  } 
}

export class KEY {
  static readonly LEFT = 37;
  static readonly UP = 38;
  static readonly RIGHT = 39;
  static readonly DOWN = 40;
}

export class NOTIFICATIONS {

  static readonly youDead = {
    title: "!Has muerto!",
    text: "Presiona continuar para regresar a la pantalla principal",
    url: "./assets/images/sin-vida.png"
  }

  static readonly youWin = {
    title: "Has completado sastifactoriamente el juego.",
    text: "Presiona continuar para regresar a la pantalla principal",
    url: "./assets/images/premio.png"
  }

}

export class MODELS {
  static readonly EMPTY = 0;
  static readonly SUPERHERO = 1;
  static readonly MONSTER = 2;
  static readonly HOLES = 3;
  static readonly GOLD = 4;
  static readonly AIR = 5;
  static readonly VISITED = 6;
  static readonly STINK = 7;
  static readonly MONSTERDEAD = 8;
}


export class CONSTROLSDESING {

  static readonly goTurnBackLeft = 'assets/images/girar-izquierda.png';
  static readonly goRunTop = 'assets/images/up.png';
  static readonly goTurnBackRight = 'assets/images/girar-derecha.png';
  static readonly goRunLeft = 'assets/images/izquierda.png';
  static readonly goTakeSomenthing = 'assets/images/agarrar.png';
  static readonly goRunRight = 'assets/images/derecha.png';
  static readonly goShoot = 'assets/images/shoot.png';
  static readonly goRunDown = 'assets/images/down.png';
  

  static readonly Legend = [
    {url: CONSTROLSDESING.goTurnBackLeft, text: "Girar 90 grados a la izquierda"},
    {url: CONSTROLSDESING.goRunTop, text: "Moverse arriba"},
    {url: CONSTROLSDESING.goTurnBackRight, text: "Girar 90 grados a la derecha"},
    {url: CONSTROLSDESING.goRunLeft, text: "Moverse a la izquierda"},
    {url: CONSTROLSDESING.goTakeSomenthing, text: "Tomar algo del suelo, si existe"},
    {url: CONSTROLSDESING.goRunRight, text: "Moverse a la derecha"},
    {url: CONSTROLSDESING.goShoot, text: "Disparar un flecha"},
    {url: CONSTROLSDESING.goRunDown, text: "Moverse abajo"},
  ]

  static readonly top = "top"
  static readonly left = "left"
  static readonly right = "right"
  static readonly down = "down"

  static readonly pressRight = "goTurnBackRight" 
  static readonly pressLeft =  "goTurnBackLeft"

  static readonly RULESTURNBACK = [
    {direction: CONSTROLSDESING.top, press:CONSTROLSDESING.pressRight, result: CONSTROLSDESING.right},
    {direction: CONSTROLSDESING.top, press:CONSTROLSDESING.pressLeft, result: CONSTROLSDESING.left},
    {direction: CONSTROLSDESING.left, press:CONSTROLSDESING.pressRight, result: CONSTROLSDESING.top},
    {direction: CONSTROLSDESING.left, press:CONSTROLSDESING.pressLeft, result: CONSTROLSDESING.down},
    {direction: CONSTROLSDESING.right, press:CONSTROLSDESING.pressRight, result: CONSTROLSDESING.down},
    {direction: CONSTROLSDESING.right, press:CONSTROLSDESING.pressLeft, result: CONSTROLSDESING.top},
    {direction: CONSTROLSDESING.down, press:CONSTROLSDESING.pressRight, result: CONSTROLSDESING.left},
    {direction: CONSTROLSDESING.down, press:CONSTROLSDESING.pressLeft, result: CONSTROLSDESING.right},
  ]

}

export class SUPERHERODESINGS {

  static readonly MONSTER = 'assets/images/monster.png';
  static readonly HOLE = 'assets/images/hoyo.png';
  static readonly S = 'assets/images/muneco.png';
  static readonly SO = 'assets/images/muneco-oro.png';
  static readonly SH = 'assets/images/muneco-hedor.png';
  static readonly SA = 'assets/images/muneco-aire.png';
  static readonly SOH = 'assets/images/muneco-hedor-oro.png';
  static readonly SOA = 'assets/images/muneco-aire-oro.png';
  static readonly SAH = 'assets/images/muneco-aire-hedor.png';
  static readonly SAHO = 'assets/images/muneco-aire-hedor-oro.png';

  static readonly RULES = [
    {desing: SUPERHERODESINGS.S, valid: [MODELS.MONSTERDEAD]},
    {desing: SUPERHERODESINGS.MONSTER, valid: [MODELS.MONSTER]},
    {desing: SUPERHERODESINGS.HOLE, valid: [MODELS.HOLES]},
    {desing: SUPERHERODESINGS.SAHO, valid: [MODELS.AIR, MODELS.GOLD, MODELS.STINK]},
    {desing: SUPERHERODESINGS.SOA, valid: [MODELS.AIR, MODELS.GOLD]},
    {desing: SUPERHERODESINGS.SOH, valid: [MODELS.GOLD, MODELS.STINK]},
    {desing: SUPERHERODESINGS.SAH, valid: [MODELS.AIR, MODELS.STINK]},
    {desing: SUPERHERODESINGS.SA, valid: [MODELS.AIR]},
    {desing: SUPERHERODESINGS.SO, valid: [MODELS.GOLD]},
    {desing: SUPERHERODESINGS.SH, valid: [MODELS.STINK]},
  ]

  static readonly Legend = [
    {url: SUPERHERODESINGS.MONSTER, text: "Has encontrado un WUMPU"},
    {url: SUPERHERODESINGS.HOLE, text: "Has encontrado un hoyo"},
    {url: SUPERHERODESINGS.SAHO, text: "Te estás posicionando con aire, hedor y oro"},
    {url: SUPERHERODESINGS.SOA, text: "Te estás posicionando con aire, y oro"},
    {url: SUPERHERODESINGS.SOH, text: "Te estás posicionando con hedor y oro"},
    {url: SUPERHERODESINGS.SAH, text: "Te estás posicionando con aire, y hedor"},
    {url: SUPERHERODESINGS.SA, text: "Te estás posicionando con aire"},
    {url: SUPERHERODESINGS.SO, text: "Te estás posicionando con oro"},
    {url: SUPERHERODESINGS.SH, text: "Te estás posicionando con hedor"},
  ]

}

export class ENVIROMENTDESINGS {

  static readonly E = 'assets/images/empty.png';
  static readonly MONSTER = 'assets/images/monster.png';
  static readonly MONSTERDEAD = 'assets/images/monsterdead.png';
  static readonly HOLE = 'assets/images/hoyo.png';
  static readonly O = 'assets/images/oro.png';
  static readonly H = 'assets/images/hedor.png';
  static readonly A = 'assets/images/aire.png';
  static readonly OH = 'assets/images/hedor-oro.png';
  static readonly OA = 'assets/images/aire-oro.png';
  static readonly AH = 'assets/images/aire-hedor.png';
  static readonly AHO = 'assets/images/aire-hedor-oro.png';

  static readonly RULES = [
    {desing: ENVIROMENTDESINGS.MONSTERDEAD, valid: [MODELS.MONSTERDEAD]},
    {desing: ENVIROMENTDESINGS.MONSTER, valid: [MODELS.MONSTER]},
    {desing: ENVIROMENTDESINGS.HOLE, valid: [MODELS.HOLES]},
    {desing: ENVIROMENTDESINGS.AHO, valid: [MODELS.AIR, MODELS.GOLD, MODELS.STINK]},
    {desing: ENVIROMENTDESINGS.OA, valid: [MODELS.AIR, MODELS.GOLD]},
    {desing: ENVIROMENTDESINGS.OH, valid: [MODELS.GOLD, MODELS.STINK]},
    {desing: ENVIROMENTDESINGS.AH, valid: [MODELS.AIR, MODELS.STINK]},
    {desing: ENVIROMENTDESINGS.A, valid: [MODELS.AIR]},
    {desing: ENVIROMENTDESINGS.O, valid: [MODELS.GOLD]},
    {desing: ENVIROMENTDESINGS.H, valid: [MODELS.STINK]},
  ]

  static readonly Legend = [
    {url: ENVIROMENTDESINGS.AHO, text: "Visitaste una casilla con oro, hedor y aire"},
    {url: ENVIROMENTDESINGS.OA, text: "Visitaste una casilla con oro, y aire"},
    {url: ENVIROMENTDESINGS.OH, text: "Visitaste una casilla con oro, y hedor"},
    {url: ENVIROMENTDESINGS.AH, text: "Visitaste una casilla con hedor y aire"},
    {url: ENVIROMENTDESINGS.A, text: "Visitaste una casilla con aire"},
    {url: ENVIROMENTDESINGS.O, text: "Visitaste una casilla con oro"},
    {url: ENVIROMENTDESINGS.H, text: "Visitaste una casilla con hedor"},
  ]

}