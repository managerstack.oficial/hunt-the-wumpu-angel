import { TestBed } from '@angular/core/testing';

import { GameService } from './game.service';
import { SUPERHERODESINGS, CONSTROLSDESING, MODELS, KEY } from './constants'

describe('GameService', () => {
  let service: GameService;

  let lives = 1;
  let monster = 5;
  let holes = 10;
  let golds = 2;
  let arrow = 4;
  let x = 15;
  let y = 10;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GameService);
  });

  function startGame(){
    service.audioEnable = false;
    service.gameRestart();
    service.setLives(lives);
    service.setMonsters(monster);
    service.setHoles(holes);
    service.setGolds(golds);
    service.setArrows(arrow);
    service.setEnvironmentX(x);
    service.setEnvironmentY(y);
    return service.gamePlay();
  }

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be generate game board and models holes, golds, air, stinks and monsters', () => {
    

    let hero = startGame();
  
    expect(service.lives).toBe(lives);
    expect(service.boardModels.monstersNumbers).toBe(monster);
    expect(service.boardModels.holesPercents).toBe(holes);
    expect(service.boardModels.goldNumber).toBe(golds);
    expect(service.arrows).toBe(arrow);
    expect(0).toBeLessThan(service.boardlogic[0].length);
    expect(0).toBeLessThan(service.boardlogic[0][0].length);
    expect(0).toBe(hero.x);
    expect((y-1)).toBe(hero.y);
    expect(SUPERHERODESINGS.S).toBe(hero.url);

  });

  it('must be able to turn the hero', () => {
    
    startGame();
    let actualDirection = service.direction;
    service.gameTurnBack(CONSTROLSDESING.pressLeft);
    expect(actualDirection).not.toBe(service.direction);
  
  });

  it('must be able to move the hero', () => {
    
    lives = 1000;
    let p = startGame();
    service.gameKeyboardActions(KEY.UP);
    expect((--p.y)).toBe(service.y);

    service.gameKeyboardActions(KEY.UP);
    expect((--p.y)).toBe(service.y);

    service.gameKeyboardActions(KEY.DOWN);
    expect((++p.y)).toBe(service.y);

    service.gameKeyboardActions(KEY.DOWN);
    expect((++p.y)).toBe(service.y);

    service.gameKeyboardActions(KEY.UP);
    expect((--p.y)).toBe(service.y);

    service.gameKeyboardActions(KEY.RIGHT);
    expect((++p.x)).toBe(service.x);

    service.gameKeyboardActions(KEY.RIGHT);
    expect((++p.x)).toBe(service.x);

    service.gameKeyboardActions(KEY.DOWN);
    expect((++p.y)).toBe(service.y);


  });


  it('should be able to collect gold', () => {
    
    startGame();

    //put my hero in the gold
    for (let ix = 0; ix < x; ix++) {
      for (let iy = 0; iy < y; iy++) {
          if (service.boardlogic[ix][iy].includes(MODELS.GOLD)){
            service.boardModels.newVisited.x = ix;
            service.boardModels.newVisited.y = iy;
            break; 
          }
      }
    }
    //Get Gold
    service.gameGetGold();
    
    //Get Logic Board position and check gold
    let p = service.boardlogic[service.boardModels.newVisited.x][service.boardModels.newVisited.y]
    expect(false).toBe(p.includes(MODELS.GOLD));
  
  });

  it('should be able to shoot all arrows', () => {
    
    startGame();
    for (let ix = 0; ix < 1000; ix++) service.gameLaunchArrow();
    expect(0).toBe(service.arrows);
  
  });

});
