import {  
  KEY, 
  Character,
  MODELS,
  CONSTROLSDESING } from './constants';

export class SuperHeroService implements Character{

  lives: number = 0;
  arrows: number = 0;
  x: number = 0;
  y: number = 0;
  xLast: number = 0;
  yLast: number = 0;
  environment: Array<number> = [];
  direction: string = CONSTROLSDESING.top;
  audioEnable = true;
 
  moves: any = {
    [KEY.LEFT]:  { x: -1, y: 0},
    [KEY.RIGHT]:  { x: 1, y: 0 },
    [KEY.DOWN]:  { x: 0, y: 1 },
    [KEY.UP]:  { x: 0, y: -1}
  };

  gameSetSound(mp3: string){
    try{
      if (this.audioEnable){
        const audio = new Audio(mp3);
        audio.play();
      }    
    }catch{}
  }

  setsSuperHeroEnviromentActual(value: number[]): void {
    this.environment = value
  }

  setSuperHeroUbication(x: number, y: number){
    this.x = x;
    this.y = y;
  }

  setSuperHeroDirection(event: string | undefined): boolean {

    if (event == undefined){
      this.direction = CONSTROLSDESING.top;
      return true
    }

    if (event == CONSTROLSDESING.pressRight || event == CONSTROLSDESING.pressLeft){
        for(const rule in CONSTROLSDESING.RULESTURNBACK){

          if (this.direction == CONSTROLSDESING.RULESTURNBACK[rule].direction){
            if (event == CONSTROLSDESING.RULESTURNBACK[rule].press){
              this.direction = CONSTROLSDESING.RULESTURNBACK[rule].result;
              return true;
            }
          }
        }
    }
    return false
  }

  /** 
   * Set the movement in the environment
  */
  setSuperHeroMoves(x: number, y: number, enviroment: number [][][]) {
    
    this.xLast = this.x;
    this.yLast = this.y;
    this.x = this.x + x;
    this.y = this.y + y;

    //Check model visited and set it if don't have it
    if (!(enviroment[this.x][this.y].includes(MODELS.VISITED)))
        enviroment[this.x][this.y].push(MODELS.VISITED);
        this.setsSuperHeroEnviromentActual(enviroment[this.x][this.y]);

  }

  /** 
   * Set the last position superhero
  */
  getSuperHeroLastVisitedZone(): {x: number, y: number}{
    return { x: this.xLast, y: this.yLast};
  }

  /** 
   * Set the actual position superhero
  */
  getSuperHeroActualZone(): { x: any; y: any;}{

    return { x: this.x, y: this.y};
  }


  setSuperHeroLives(movements: { x: number; y: number; }, enviroment: number[][][]){
  
    let tempX = this.x + movements.x;
    let tempY = this.y + movements.y;

    if (((enviroment[tempX][tempY].includes(MODELS.MONSTER)) || 
        (enviroment[tempX][tempY].includes(MODELS.HOLES))) && 
        (!enviroment[tempX][tempY].includes(MODELS.MONSTERDEAD))){
      
      this.gameSetSound('assets/mp3/dead.mp3');
      this.lives -= 1;

      if (this.lives == 0)
        this.gameSetSound('assets/mp3/lost.mp3');
          
    }
  }


  getSuperHeroLaunchArrow(x: number, y: number, cols: number, rows: number, behaviorEnvironment: Function):{
      x: number;
      y: number;
  } | {
      x: undefined;
      y: undefined;
  } {

  if (this.direction == CONSTROLSDESING.top) return this.getSuperHeroLaunchTop(x, y, behaviorEnvironment);
  else if (this.direction == CONSTROLSDESING.right) return this.getSuperHeroLaunchRight(x, y, cols, behaviorEnvironment);
  else if (this.direction == CONSTROLSDESING.left) return this.getSuperHeroLaunchLeft(x, y, behaviorEnvironment);
  else if (this.direction == CONSTROLSDESING.down) return this.getSuperHeroLaunchDown(x, y, rows, behaviorEnvironment);

  return {x: undefined, y: undefined}

}

getSuperHeroLaunchTop(x: number, y: number, behaviorEnvironment: Function){
  for (y; y >= 0; y--) if (behaviorEnvironment(x, y)) return {x: x, y: y}
  return {x: undefined, y: undefined}
}

getSuperHeroLaunchRight(x: number, y: number, cols: number, behaviorEnvironment: Function){
  for (let i = x; i < cols; i++) if (behaviorEnvironment(i, y)) return {x: i, y: y}
  return {x: undefined, y: undefined}
}

getSuperHeroLaunchLeft(x: number, y: number, behaviorEnvironment: Function){
  for (x; x >= 0; x--) if (behaviorEnvironment(x, y)) return {x: x, y: y}
  return {x: undefined, y: undefined}
}

getSuperHeroLaunchDown(x: number, y: number, rows: number, behaviorEnvironment: Function){
  for (let i = y; i < rows; i++) if (behaviorEnvironment(x, i)) return {x: x, y: i}
  return {x: undefined, y: undefined}
}

}