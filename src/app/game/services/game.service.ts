import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SuperHeroService } from './superhero.service';
import {  MODELS, 
          boardModelsDefault,
          RenderPosition,
          SUPERHERODESINGS, 
          ENVIROMENTDESINGS} from './constants';

const BLOCK_SIZE = 60;


@Injectable({
  providedIn: 'root'
})
export class GameService extends SuperHeroService {

  //Logic space in memory  
  boardModels = {...boardModelsDefault}
  boardlogic: number[][][] = [[[]]];
  

 
  private dataBoardSubject: BehaviorSubject<object> = new BehaviorSubject({});
  public readonly dataBoard: Observable<object>  = this.dataBoardSubject.asObservable();

  updateObservable(){
    return this.dataBoardSubject.next(this.boardModels);
  }


  setDataBoardNewPosition(visited: RenderPosition, new_: RenderPosition){

    this.boardModels = {
      ...this.boardModels, 
      lives: this.lives,
      direction: this.direction, 
      arrows: this.arrows,
      lastVisited:visited,
      newVisited:new_,
    };

    this.updateObservable();
  }

  setDataBoardStandar(object: string, value: number | string | boolean){
    this.boardModels = {
      ...this.boardModels,
      [object]: value
    } 
    this.updateObservable();
  }

  setLives(value: number){
    this.lives = value;
    this.boardModels.lives = this.lives;
  }

  setMonsters(value: number){
    this.boardModels.monstersNumbers = value;
  }

  setHoles(value: number){
    this.boardModels.holesPercents = value;
  }
  
  setGolds(value: number){
    this.boardModels.goldNumber = value;
  }

  setArrows(value: number){
    this.arrows = value;
  }

  setEnvironmentX(value: number){
    this.boardModels.cols = value;
  }

  setEnvironmentY(value: number){
    this.boardModels.rows = value;
  }

  getEnvironmentX(){
    return this.boardModels.cols
  }

  getEnvironmentY(){
    return this.boardModels.rows
  }
  
  getBlockSize(){
    return BLOCK_SIZE;
  }

  /**
   * Clear all board, and set the escenario
   * @returns Render position send the initials params to the Superhero
   */
  gamePlay(): RenderPosition{
    
    let p = {
      x: 0, 
      y: (this.boardModels.rows-1), 
      url: SUPERHERODESINGS.S
    }

    this.setGameLogicalEmptyBoard();
    this.setGameLogicalModels();
    this.setSuperHeroUbication(p.x, p.y);
    this.setSuperHeroDirection(undefined);
    this.setDataBoardNewPosition(p, p);

    return p
  }

  gameRestart(){
    this.boardModels = {...boardModelsDefault}
  }


  /**
   * Check the movements to the SuperHero
   * @param event get event from the keyboard
   * @returns undefined in case the actions it is not in the availables moves, 
   * else return the last position and the new position
   */
  gameKeyboardActions(code: number): boolean {

    let newHeroMovementAndEnviornment;
    let visitedMovementAndEnviornment;
    let movements = this.moves[code] || undefined;

    //check we have lives and movement are valids
    if (movements == undefined || this.lives == 0 )  return false
    
    //check if we have access a new space else return undefined
    if (this.gameGetWall(movements)) return false

    //check our lives and calculate if our move was danger
    this.setSuperHeroLives(movements, this.boardlogic);

    //set the move in the super hero
    this.setSuperHeroMoves(movements.x, movements.y, this.boardlogic);

    newHeroMovementAndEnviornment = this.getGameLogicalZoneModels(
      SUPERHERODESINGS.RULES, 
      this.getSuperHeroActualZone(),
      SUPERHERODESINGS.S);

    visitedMovementAndEnviornment = this.getGameLogicalZoneModels(
      ENVIROMENTDESINGS.RULES, 
      this.getSuperHeroLastVisitedZone(),
      ENVIROMENTDESINGS.E);
    
    this.setDataBoardNewPosition(visitedMovementAndEnviornment, newHeroMovementAndEnviornment);
    this.gameGetIsWin();
    return true
  }

  gameGetWall(movements: { x: number; y: number; }): boolean {

    if ((this.x == 0 && movements.x == -1) ||
        (this.y == 0 && movements.y == -1) ||
        (this.y >= (this.boardModels.rows - 1) && movements.y == 1) ||
        (this.x >= (this.boardModels.cols - 1) && movements.x == 1)){
      this.gameSetSound('assets/mp3/walls.mp3');
      return true
    }
    return false;
  }



  gameGetGold(){

    let x = this.boardModels.newVisited.x
    let y = this.boardModels.newVisited.y

    if (this.boardlogic[x][y].includes(MODELS.GOLD)){
      let index = this.boardlogic[x][y].indexOf(MODELS.GOLD);
      this.boardlogic[x][y].splice(index, 1);
      this.boardModels.goldTaken += 1;
    }
  }

  gameLaunchClear(){
    this.boardModels.lastLaunch = {x:0, y:0, url:''};
  }

  gameLaunchArrow(): boolean{

    if (this.boardModels.arrows == 0)
      return false
    

    this.gameSetSound('assets/mp3/arrow.mp3');
    let {x, y} = this.getSuperHeroLaunchArrow(
        this.boardModels.newVisited.x,
        this.boardModels.newVisited.y,
        this.boardModels.cols,
        this.boardModels.rows,
        this.getGameCheckMoster.bind(this));

    if (x != undefined && y != undefined){
        
        this.boardlogic[x][y].push(MODELS.MONSTERDEAD);
        this.boardModels.lastLaunch = this.getGameLogicalZoneModels(ENVIROMENTDESINGS.RULES, {x, y},ENVIROMENTDESINGS.E);
        this.gameSetSound('assets/mp3/monsterdead.mp3');
        console.log(this.boardlogic[x][y])
    }
    console.log(x, y)

    this.boardModels.arrows = this.arrows -= 1;
    this.updateObservable();
    return true

  }

  getGameCheckMoster(x: number, y: number): boolean {

     if( this.boardlogic[x][y].includes(MODELS.MONSTER) && 
        (!this.boardlogic[x][y].includes(MODELS.MONSTERDEAD)))
      return true
    return false
  }


  gameTurnBack(side: string){
    this.setSuperHeroDirection(side);
    this.setDataBoardStandar("direction", this.direction);
  }

  gameGetIsWin(): boolean{
    let x = this.x
    let y = this.y
    if (this.boardModels.goldTaken == this.boardModels.goldNumber && x == 0 && y == (this.boardModels.rows-1)){
      this.setDataBoardStandar("win", true);
      this.gameSetSound('assets/mp3/winner.mp3');
      return true
    }
    return false
  }

  /************************* */
  /** MANAGER LOGICAL BOARD */

  /**
   * Controller to generate all logical models
   */
  private setGameLogicalModels(){

    this.setGameLogicalModelsGolds();
    this.setGameLogicalModelsMonsters();
    this.setGameLogicalModelsHolesAndAir(); 
  
  }

  /**
   * Set position monsters and stinks in the enviroment
   */
  setGameLogicalModelsMonsters(){

    let isPosible = true;
    let monstersX = Math.ceil((Math.random() * ( this.boardModels.cols - 1)));
    let monstersY = Math.ceil((Math.random() * ( this.boardModels.rows - 1)));
    
    if (this.boardlogic[monstersX][monstersY].includes(MODELS.HOLES) || 
        this.boardlogic[monstersX][monstersY].includes(MODELS.GOLD))
        isPosible = false;
    
    if (isPosible){
      this.boardModels.monsters += 1;
      this.setGameAssingArea(monstersX, monstersY, MODELS.MONSTER);
      this.setGameAssingArea(monstersX-1, monstersY, MODELS.STINK);
      this.setGameAssingArea(monstersX+1, monstersY, MODELS.STINK);
      this.setGameAssingArea(monstersX, monstersY+1, MODELS.STINK);
      this.setGameAssingArea(monstersX, monstersY-1, MODELS.STINK);
    }

    if (this.boardModels.monsters < this.boardModels.monstersNumbers )
      this.setGameLogicalModelsMonsters();

  }

  /**
   * Set position gold in the enviroment
   */
  setGameLogicalModelsGolds(){


    let isPosible = true;
    let goldX = Math.ceil((Math.random() * (this.boardModels.cols - 1)));
    let goldY = Math.ceil((Math.random() * (this.boardModels.rows - 1)));

    if (this.boardlogic[goldX][goldY].includes(MODELS.HOLES) || 
        this.boardlogic[goldX][goldY].includes(MODELS.MONSTER))
        isPosible = false;
    
    if (isPosible){
      this.boardModels.gold += 1;
      this.boardlogic[goldX][goldY].push(MODELS.GOLD);
    }

    if (this.boardModels.gold < this.boardModels.goldNumber)
      this.setGameLogicalModelsGolds();
      
  }

  /**
   * Set position Holes and Air in the enviroment
   */
  setGameLogicalModelsHolesAndAir(){


    for(var x = 0; x < this.boardModels.cols; x++) {
      for(var y = 0; y < this.boardModels.rows; y++) {

        //Empty initial spaces
        if ((x == 0 && y == this.boardModels.rows) || 
            (x == 0 && y == (this.boardModels.rows - 1)) || 
            (x == 1 && y == this.boardModels.rows) || 
            (x == 1 && y == (this.boardModels.rows - 1)))
          continue;
        
        //Check anothers Models in the site
        if (this.boardlogic[x][y].includes(MODELS.GOLD) || 
          this.boardlogic[x][y].includes(MODELS.MONSTER)){
          continue;
        }
        
        var randNumber = Math.random() * 100;
        
        //if the percentage match is valid assing holes, and air
        if (randNumber <= this.boardModels.holesPercents){
          this.setGameAssingArea(x, y, MODELS.HOLES);
          this.setGameAssingArea(x-1, y, MODELS.AIR);
          this.setGameAssingArea(x+1, y, MODELS.AIR);
          this.setGameAssingArea(x, y+1, MODELS.AIR);
          this.setGameAssingArea(x, y-1, MODELS.AIR);
        }
      }
    }
  }

  /**
   * Create a logical board empty
   */
  private setGameLogicalEmptyBoard() {
    for(var x = 0; x < this.boardModels.cols; x++) {
      this.boardlogic[x] = new Array();
      for(var y = 0; y < this.boardModels.rows; y++) {
        this.boardlogic[x][y] = new Array();
        this.boardlogic[x][y].push(MODELS.EMPTY);
      }
    }
  }


  /**
   * Generic assing of model in the board
   */
  private setGameAssingArea(x: number, y: number, model:number): Boolean{

    if (x < 0) return false;
    if (x > ( this.boardModels.cols - 1)) return false;
    if (y < 0) return false;
    if (y > ( this.boardModels.rows - 1)) return false;
    if (this.boardlogic[x][y].includes(model)) return false;

    this.boardlogic[x][y].push(model)

    return true;

  }


  /**
   * Generate a render positions with x, y and desing
   * @param rules   rules in the enviroment
   * @param position positions actuals 
   * @param urlDefault default desing
   * @returns 
   */
  private getGameLogicalZoneModels(
    rules: { desing: string; valid: number[]; }[],
    position: {x: number; y: number; },
    urlDefault: string): RenderPosition{
    let x = position.x;
    let y = position.y;
    let url = this.getDesingFromEnviroment(rules, this.boardlogic[x][y], urlDefault);
    return {
      x,
      y,
      url, 
    }
  }

  /**
   * Check the rules and select the desing
   */
  private getDesingFromEnviroment(
    rules: { desing: string; valid: number[]; }[],
    zone: number[],
    defaultDesing: string){

    let desing = defaultDesing;
    for (const rule in rules ){

      let allPassed = rules[rule].valid.every((e: number) => {
        return zone.includes(e);
      });
      
      if (allPassed){
        desing = rules[rule].desing;
        break;
      }
    }

    return desing;
  }
}