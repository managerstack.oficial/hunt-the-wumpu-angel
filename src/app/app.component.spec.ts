import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { GameService } from './game/services/game.service';
import { GameData } from './game/services/constants'

import { 
  HomeComponent, 
  GameBoardComponent,
  ControlPanelComponent,
  ControlPanelLegendComponent,
  ControlPanelStatiticsComponent,
  ControlPanelControlsComponent,
  ControlPanelEndComponent
 } from './game/components';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let gameService: GameService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HomeComponent, 
        GameBoardComponent,
        ControlPanelComponent,
        ControlPanelLegendComponent,
        ControlPanelStatiticsComponent,
        ControlPanelControlsComponent,
        ControlPanelEndComponent
      ],
      imports: [
        FormsModule
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should render the component configuration game', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.home-container')).not.toEqual(null);
  });
});
